const readline = require("readline");
const chalk = require("chalk");
const gameEngine = require("@nunosantos/game-engine");
const easy = 1;
const medium = 2;
const hard = 3;
var hasChosen;

const free =  `
  +---+
  |   |
      |
  `+ chalk.green('\\O/') +` |
   `+ chalk.green('|') +`  |
  ` + chalk.green('/ \\') +` |
=========`;

const HANGMANPICS = [`
  +---+
  |   |
      |
      |
      |
      |
=========`, `
  +---+
  |   |
  O   |
      |
      |
      |
=========`, `
  +---+
  |   |
  O   |
  |   |
      |
      |
=========`, `
  +---+
  |   |
  O   |
 /|   |
      |
      |
=========`, `
  +---+
  |   |
  O   |
 /|\\  |
      |
      |
=========`, `
  +---+
  |   |
  O   |
 /|\\  |
 /    |
      |
=========`, `
  +---+
  |   |
  O   |
 /|\\  |
 / \\  |
      |
=========`, `
  +---+
  ` + chalk.red ("|") + `   |
  ` + chalk.red ("o") + `   |
 ` + chalk.red ("|||") + `  |
 ` + chalk.red ("| |") + `  |
      |
=========`];

const linereader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let gameState = null;


const run = () => {
  gameState = gameEngine.startGame ();
  console.log("*********************");
  console.log("*     NOVO JOGO     *");
  console.log("*********************");
  chooseLevel();
};

function computeLevel (gameState, number){
  switch (number) {
    case easy:
    gameState.lives=6;
    gameState.chosenLevel=easy;
    hasChosen=true;
    break;

    case medium:
    gameState.lives=4;
    gameState.chosenLevel=medium;
    hasChosen=true;
    break;

    case hard:
    gameState.lives=2;
    gameState.chosenLevel=hard;
    hasChosen=true;
    break;
  }
  
  return {
    has_Chosen: hasChosen,
    game_State: gameState
  }
}

function chooseLevel() {
  linereader.question("Escolhe o nível: 1 - Fácil; 2 - Médio; 3 - Difícil" + "\n", number => {
    var obj = computeLevel(gameState, parseInt(number, 10));    
    hasChosen = obj.has_Chosen;
    gameState = obj.game_State;

    if (hasChosen) {
      game_loop(gameState);
    } else {
      chooseLevel();
    }
  });
}

const game_loop = game => { 
  printGameState(game);  
  evaluateGuess(game);
};

const printGameState = game => {
  console.log("Nº de vidas: " + game.lives);
  console.log("Palavra: " + game.display_word);
  console.log("Letras usadas: " + game.guesses);
  console.log (HANGMANPICS[6 - game.lives]);
  console.log("\n\n");
};

const printGameWin = game => {
  console.log("Ganhaste!!!! :)");
  console.log("Palavra original: " + game.word);
  console.log(free);
  console.log("\n\n\n");
};

const printGameLost = game => {
  console.log("Já foste!!!! :(");
  console.log("Palavra original: " + game.word);
  console.log (HANGMANPICS[6 - game.lives]);
  console.log("\n\n\n");
};

const evaluateGuess = game => {
  linereader.question("Escolhe uma letra: ", letter => {
    if (letter) {
      game = gameEngine.takeGuess (game, letter);
    }
    if (game.status == "DEAD" || game.lives < 0) {
      printGameLost (game);
      run ();
    } else if (game.status == "WIN") {
      printGameWin (game);
      run ();
    } else {
      game_loop(game);
    }
  });
};

module.exports = {
  run
};
