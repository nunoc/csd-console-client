const GameEngine = require("../lib/game_engine");
let gameEngine = GameEngine.startGame ();

describe("The Game Engine must", () => {
  test("start a new game with a random word", () => {
    console.log ("|" + gameEngine.word + "|");
    expect (gameEngine.status).toBe ("RUNNING");
    expect (gameEngine.lives).toBe (5);
    expect (gameEngine.display_word.length).toBe (gameEngine.word.length * 2 - 1);
    expect (gameEngine.guesses).toHaveLength (0);
  });

  /*
  test("update the guesses and lives when a wrong guess is given", () => {
    gameEngine.takeGuess (gameEngine, "");
    gameEngine.takeGuess (gameEngine, null);
    gameEngine.takeGuess (gameEngine, "A");
  });
  */

  test("test all the possibilities with dead awaiting", () => {
    gameEngine.word = "bajoras";
    let result = GameEngine.takeGuess (gameEngine, "J");
    expect(result.guesses.length).toBe(1); // Guess one right
    expect(result.lives).toBe(5); // Guess one right
    result = GameEngine.takeGuess (result, "A");
    expect(result.guesses.length).toBe(2); // Guess multiple right
    expect(result.lives).toBe(5); // Guess one right
    result = GameEngine.takeGuess (result, "Y");
    expect(result.guesses.length).toBe(3); // Guess one wrong
    expect(result.lives).toBe(4); // Guess one right
    result = GameEngine.takeGuess (result, "A");
    expect(result.guesses.length).toBe(3); // Guess one that has already been found
    expect(result.lives).toBe(4); // Guess one that has already been found
    result = GameEngine.takeGuess (result, "Q");
    expect(result.guesses.length).toBe(4); // Guess one wrong
    expect(result.lives).toBe(3); // Guess one wrong
    result = GameEngine.takeGuess (result, "W");
    expect(result.guesses.length).toBe(5); // Guess one wrong
    expect(result.lives).toBe(2); // Guess one wrong
    result = GameEngine.takeGuess (result, "E");
    expect(result.guesses.length).toBe(6); // Guess one wrong
    expect(result.lives).toBe(1); // Guess one wrong
    result = GameEngine.takeGuess (result, "T");
    expect(result.guesses.length).toBe(7); // Guess one wrong
    expect(result.lives).toBe(0); // Guess one wrong

    // DEAD
    result = GameEngine.takeGuess (result, "U");
    expect(result.guesses.length).toBe(8); // Guess one wrong
    expect(result.lives).toBe(-1); // Guess one wrong
  });

  test("test all the possibilities with glory awaiting", () => {
    gameEngine.word = "bajoras";
    let result = GameEngine.takeGuess (gameEngine, "J");
    expect(result.guesses.length).toBe(1); // Guess one right
    expect(result.lives).toBe(5); // Guess one right
    result = GameEngine.takeGuess (result, "A");
    expect(result.guesses.length).toBe(2); // Guess multiple right
    expect(result.lives).toBe(5); // Guess one right
    result = GameEngine.takeGuess (result, "B");
    expect(result.guesses.length).toBe(3); // Guess one wrong
    expect(result.lives).toBe(5); // Guess one right
    result = GameEngine.takeGuess (result, "A");
    expect(result.guesses.length).toBe(3); // Guess one that has already been found
    expect(result.lives).toBe(5); // Guess one that has already been found
    result = GameEngine.takeGuess (result, "O");
    expect(result.guesses.length).toBe(4); // Guess one wrong
    expect(result.lives).toBe(5); // Guess one wrong
    result = GameEngine.takeGuess (result, "R");
    expect(result.guesses.length).toBe(5); // Guess one wrong
    expect(result.lives).toBe(5); // Guess one wrong

    // GLORY
    result = GameEngine.takeGuess (result, "S");
    expect(result.guesses.length).toBe(6); // Guess one wrong
    expect(result.lives).toBe(5); // Guess one wrong
  });

  
});
